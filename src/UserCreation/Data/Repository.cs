﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using UserCreation.Models;

namespace UserCreation.Data
{
    public class Repository : IRepository
    {
        private SqlConnection _connection;

        public Repository(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
        }

        public bool Create(User toAdd)
        {
            var command = _connection.CreateCommand();
            command.CommandText = $"INSERT INTO USERS (ID, Username, Password) VALUES ('{toAdd.Id.ToString()}', '{toAdd.Username}', '{toAdd.Password}')";

            try
            {
                _connection.Open();

                return Convert.ToBoolean(command.ExecuteNonQuery());

            }
            catch(SqlException sqlException)
            {
                //Log the exception
                Console.WriteLine(sqlException.Message);
                return false;
            }
            finally
            {
                _connection.Close();
            }

        }
    }
}
