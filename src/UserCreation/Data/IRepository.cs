﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserCreation.Models;

namespace UserCreation.Data
{
    public interface IRepository
    {
        bool Create(User toAdd);
    }
}
