﻿// Write your Javascript code.
$(document).ready(function () {
    $('#userCreationForm').validate({
        rules: {
            Username: {
                required: true
            },
            Password: {
                required: true,
                minlength: 8
            }
        }
    });
});