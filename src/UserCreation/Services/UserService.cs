﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserCreation.Data;
using UserCreation.Models;

namespace UserCreation.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository _repository;
        private readonly IPasswordService _passwordService;

        public UserService(IRepository repository, IPasswordService passwordService)
        {
            _repository = repository;
            _passwordService = passwordService;
        }

        public bool CreateUser(string username, string password)
        {
            var user = new User(username, _passwordService.BCryptPassword(password));

            return _repository.Create(user);
        }
    }
}
