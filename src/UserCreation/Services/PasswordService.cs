﻿namespace UserCreation.Services
{
    public class PasswordService : IPasswordService
    {
        private readonly int _workFactor;

        public PasswordService(int workFactor)
        {
            _workFactor = workFactor;
        }

        public string BCryptPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password, _workFactor);
        }

        public bool VerifyPassword(string password, string hash)
        {
            return BCrypt.Net.BCrypt.Verify(password, hash);
        }
    }
}
