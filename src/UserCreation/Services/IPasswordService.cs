﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserCreation.Services
{
    public interface IPasswordService
    {
        string BCryptPassword(string password);
        bool VerifyPassword(string password, string hash);
    }
}
