﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserCreation.Services
{
    public interface IUserService
    {
        bool CreateUser(string username, string password);
    }
}
