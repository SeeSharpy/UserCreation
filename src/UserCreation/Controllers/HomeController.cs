﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UserCreation.Services;
using UserCreation.ViewModels;

namespace UserCreation.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService _userService;
        public HomeController(IUserService userService)
        {
            _userService = userService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateUser(CreateUserModel model)
        {
            if (ModelState.IsValid)
            {
                if (_userService.CreateUser(model.Username, model.Password))
                {
                    return View(model);
                }

                ModelState.AddModelError("", "Error saving user to database");
                return View("Index", model);
            }

            ModelState.AddModelError("", "Missing Username / Password");
            return View("Index", model);
        }

    }
}
