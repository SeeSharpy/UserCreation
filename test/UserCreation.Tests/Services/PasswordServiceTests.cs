﻿using UserCreation.Services;
using Xunit;

namespace UserCreation.Tests.Services
{

    public class PasswordServiceTests
    {
        private const string PASSWORD = "password";

        private readonly PasswordService _passwordService;
        private readonly string _hashedPassword;


        public PasswordServiceTests()
        {
            _passwordService = new PasswordService(4);
            _hashedPassword = _passwordService.BCryptPassword(PASSWORD);
        }

        [Fact]
        public void WhenPassingInAStringADifferentStringMustBeReturned()
        {
            Assert.False(PASSWORD == _hashedPassword);
        }

        [Fact]
        public void WhenPassingCorrectPasswordItShouldVerifyAsTrue()
        {
            Assert.True(_passwordService.VerifyPassword(PASSWORD, _hashedPassword));
        }

        [Fact]
        public void WhenPassingIncorrectPasswordItShouldVerifyAsFalse()
        {
            Assert.False(_passwordService.VerifyPassword("incorrect", _hashedPassword));
        }
    }
}
