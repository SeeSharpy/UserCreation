﻿using Moq;
using System.Linq;
using System.Collections.Generic;
using UserCreation.Data;
using UserCreation.Models;
using UserCreation.Services;
using Xunit;

namespace UserCreation.Tests.Services
{
    public class UserServiceTest
    {
        private const string USERNAME = "username";
        private const string PASSWORD = "password";

        private User _user;
        private readonly Mock<IRepository> _repository = new Mock<IRepository>();
        private readonly Mock<IPasswordService> _passwordService = new Mock<IPasswordService>();
        private List<User> _users = new List<User>();
        private readonly UserService _userService;

        public UserServiceTest()
        {
            _user = new User(USERNAME, PASSWORD);

            _passwordService.Setup(x => x.BCryptPassword(It.IsAny<string>()))
                .Returns(PASSWORD);
            _repository.Setup(x => x.Create(It.IsAny<User>()))
                .Callback<User>((u) => _users.Add(u));

            _userService = new UserService(_repository.Object, _passwordService.Object);
        }

        [Fact]
        public void WhenCreatingAUserVerifyTheUsernameAndPasswordHaveBeenStored()
        {
            _userService.CreateUser(USERNAME, PASSWORD);

            Assert.True((_users.Count(u => u.Username == USERNAME && u.Password == PASSWORD)) == 1);
        }

    }
}
