﻿using System;
using System.Data.SqlClient;

namespace UserCreation.Tests
{
    public class DatabaseFixture : IDisposable
    {
        public SqlConnection Database { get; private set; }
        public DatabaseFixture()
        {
            Database = new SqlConnection(@"Server=localhost\sqlexpress;Database=UsersTest;Trusted_Connection=true;");

            //Setup test data
        }
        public void Dispose()
        {
            Database.Open();
            var command = new SqlCommand("DELETE * FROM users");

            try
            {
                Console.WriteLine("Clearing Data");
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error clearing data: {e.Message}");
            }
            finally
            {
                Database.Close();
            }
        }
    }
}
