# User Creation Tool

User creation tool, created by Robert Davies

Written in ASP.Net Core 1.0.1

Database: SQL Server 2016 Express Edition

# Instructions

1. Create Database / Table
    Run CreateDatabase.sql followed by CreateTable.sql

2. Point Connection String to correct sql server
    It was developed on SQL Server 2016 Express, if you are running a different verion please change the connection string in appsettings.json
    ```json
    "ConnectionStrings": {
        "DefaultConnection": "Server=localhost\\sqlexpress;Database=Users;Trusted_Connection=true;"
    }
    ```

3. Build and run the application

4. To run the tests, using a command window, navigate to the test directory and run `dotnet test`

# Notices

1. Database Fixture class is not currently used, it is designed to be used for integration tests, it shows how I would setup and destroy the test database, for each integration test that would use it.


